package ru.geekbrains;

public class Main {

    public static void main(String[] args) {
        Account account = Account.newBuilder()
                .setToken("rus")
                .setUserId("grack")
                .build();
        System.out.println(account.toString());

        Person person = Person.newBuilder()
                .setCity("Moscow")
                .setEmployed(false)
                .setFemale(true)
                .build();
        System.out.println(person.toString());
        Person person1 = Person.newBuilder()
                .setFemale(false)
                .setEmployed(true)
                .setCity("ekat")
                .setFirstName("jorge")
                .setHomeOwner(true)
                .setLastName("grut")
                .setMiddleName("month")
                .setSalutation("hi")
                .setState("ural")
                .setStreetAddress("street")
                .setSuffix("ra")
                .build();
        System.out.println();
        System.out.println(person1.toString());

        Account account1 = Account.newBuilder()
                .setToken("awd")
                .setUserId("qads")
                .build();

        System.out.println(account1.toString());
    }
}
